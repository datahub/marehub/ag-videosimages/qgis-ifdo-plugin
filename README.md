# QGIS iFDO Plugin 

A QGIS Python plugin to load images and metadata from iFDO files.

## Installation

* Clone with mariqt submodule:

  ``` $ git clone --recurse-submodules ``` ...

* Copy or create symbolic link to QGIS plugin folder:

  ``` $ ln -s /path/to/import_ifdo/  /home/USER/.local/share/QGIS/QGIS3/profiles/default/python/plugins/ ```

* Load plugin in QGIS:

  Plugins -> Manage and install plugins... -> Installed -> check "iFDO Import"

